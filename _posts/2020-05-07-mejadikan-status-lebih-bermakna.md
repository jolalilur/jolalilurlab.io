---
layout: post
penulis: Wahid
judul: Menjadikan Status Lebih Bermakna
---

Duluuu kata "status" bisa dibilang lebih mengarah ke status di KTP. Namun kini kebanyakan kawula milenial mendengar kata status, pasti tertuju pada status di WA, (atau *story* di IG), betul?

Awal-awal dirilis fitur status di WA itu bisa dibilang menggeser dari fungsi utama WA, yaitu sebagai aplikasi perpesanan alias *chatting*. Kini setiap hari, setiap jam, bahkan setiap menit ada bintik lingkaran di tab status.

Akan tetapi, coba kita pikir dengan akal sehat & hati yg jernih, seberapa berfaedahkah status-status itu?

Melalui tulisan ini, saya berharap agar menjadi awal status-status itu lebih bermakna.

Semoga..